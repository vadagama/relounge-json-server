// Load Chance
var Chance = require("chance");
const date = new Date();

// Instantiate Chance so it can be used
var chance = new Chance();

// index.js
module.exports = () => {
  const data = {
    members: [],
    invoices: [],
    payments: [],
    templateSettings: [],
    trainingAnalytics: [],
    appointmentHistory: [],
    appointments: [],
    anamnesis: [],
    singleRecheck: [],
    rechecks: [],
    backchecks: [],
    employees: [],
    devices: [],
    companies: [],
    locations: [],
    competences: [],
    countries: [],
    cities: [],
  };

  for (let i = 0; i < 1000; i++) {
    data.invoices.push({
      id: i,
      number: "INV" + chance.prime({ min: 100000, max: 999999 }),
      member: chance.first() + " " + chance.first(),
      date: chance.date({ string: true, year: 2022 }),
      amount: chance.floating({ min: 50, max: 2000 }),
      status: "paid",
      file: "https://cdn.brandfolder.io/AMC8F81D/at/wxct6tv9sbrcw4nzwzfcr/ZR_Quick-Start-Guide_Web.pdf",
    });
  }

  for (let i = 0; i < 1000; i++) {
    data.payments.push({
      id: i,
      serviceName: "Anamnesys",
      member: chance.first() + " " + chance.first(),
      date: chance.date({ string: true, year: 2022 }),
      invoiceNumber: "INV" + chance.prime({ min: 100000, max: 999999 }),
      amount: chance.floating({ min: 50, max: 2000 }),
      file: "https://cdn.brandfolder.io/AMC8F81D/at/wxct6tv9sbrcw4nzwzfcr/ZR_Quick-Start-Guide_Web.pdf",
    });
  }

  data.templateSettings.push({
    id: 0,
    firstName: "Oleg",
    lastName: "Krasnov",
    email: "oleg@krasnov.com",
    companyName: "Netflix",
    country: "Russia",
    city: "Moscow",
    postalCode: "123456",
    street: "Kravchenko 11",
  });

  let day = 1;
  let mounth = 1;

  for (let i = 0; i < 100; i++) {
    if (day != 30) {
      day++;
    } else {
      day = 1;
      mounth++;
    }

    if (mounth > 12) {
      mounth = 1;
    }

    var array = [
      "Stress Level",
      "Sleep Quality",
      "Upper Back",
      "Middle Back",
      "Lower Back",
      "How Affect",
      "Physical Mobility",
      "Health Status",
      "Emotional Statte",
    ];
    data.trainingAnalytics.push({
      date: mounth + "/" + day + "/" + "2022",
      value: chance.prime({ min: 0, max: 10 }),
      category: array[Math.floor(Math.random() * array.length)],
    });
  }

  for (let i = 0; i < 10; i++) {
    data.backchecks.push({
      id: i,
      url: "/assets/images/backchecks/backcheck_001.png",
      date: chance.date({ string: true, year: 2022 }),
      type: "Backcheck",
      description: "",
    });
  }

  for (let i = 0; i < 100; i++) {
    data.appointmentHistory.push({
      id: i,
      appointment: "Anamnesys",
      dateTime:
        chance.date({ string: true, year: 2022 }) +
        " " +
        chance.hour() +
        ":" +
        chance.minute() +
        "" +
        chance.ampm(),
      status: "upcoming",
    });
  }

  data.appointments.push(
    {
      id: 0,
      title: "0 1 2",
      start: new Date("2022-07-03T08:00:00.000Z"),
      end: new Date("2022-07-03T09:00:00.000Z"),
      appointmentType: "anamnesis",
      notes: chance.sentence({ words: 10 }),
      employee: chance.first() + " " + chance.last(),
      blockedDevices: [0, 1, 2],
    },
    {
      id: 1,
      title: "0 1 2",
      start: new Date("2022-07-01T08:00:00.000Z"),
      end: new Date("2022-07-01T09:00:00.000Z"),
      appointmentType: "anamnesis",
      notes: chance.sentence({ words: 10 }),
      employee: chance.first() + " " + chance.last(),
      blockedDevices: [0, 1, 2],
    }
  );

  data.anamnesis.push(
    { "Do you do any sports?": "No" },
    { "Do you have any physical problems (other than back)?": "Yes" },
    { "Are you currently receiving medical treatment?": "No" },
    { "Rate your stress level": "5" },
    { "Rate your sleep quality": "2" },
    {
      "How much does the back pain affect your quality of life?":
        "Affect a lot",
    },
    { "Do you have any specific complaints in the back pain?": "Osteoporosis" },
    { "How does your back pain manifest itself?": "Chronic" },
    { "How much does back pain affect your quality of life?": "Appears only" },
    { "Choose your goals": "Muscle building" },
    {
      "What have you done so far to achieve your goals?":
        "Had a course of treatment",
    }
  );

  data.singleRecheck.push(
    { "Rate your stress level": "5" },
    { "Rate your sleep quality": "4" },
    { "Pain Upper back": "2" },
    { "Pain Middle back": "5" },
    { "Pain lower back": "2" },
    { "How much does the back pain affect your quality of life?": "6" },
    { "Rate your physical mobility": "5" },
    { "Rate your health status in general": "2" },
    { "Rate your emotional state": "6" }
  );

  for (let i = 0; i < 10; i++) {
    data.rechecks.push(chance.date({ string: true, year: 2022 }));
  }

  data.competences.push("Administrator", "Manager", "Trainer", "Brain Fucker");

  // Create countries objects
  data.countries.push(
    "Afghanistan",
    "Albania",
    "Algeria",
    "Andorra",
    "Angola",
    "Anguilla",
    "Antigua &amp; Barbuda",
    "Argentina",
    "Armenia",
    "Aruba",
    "Australia",
    "Austria",
    "Azerbaijan",
    "Bahamas",
    "Bahrain",
    "Bangladesh",
    "Barbados",
    "Belarus",
    "Belgium",
    "Belize",
    "Benin",
    "Bermuda",
    "Bhutan",
    "Bolivia",
    "Bosnia &amp; Herzegovina",
    "Botswana",
    "Brazil",
    "British Virgin Islands",
    "Brunei",
    "Bulgaria",
    "Burkina Faso",
    "Burundi",
    "Cambodia",
    "Cameroon",
    "Cape Verde",
    "Cayman Islands",
    "Chad",
    "Chile",
    "China",
    "Colombia",
    "Congo",
    "Cook Islands",
    "Costa Rica",
    "Cote D Ivoire",
    "Croatia",
    "Cruise Ship",
    "Cuba",
    "Cyprus",
    "Czech Republic",
    "Denmark",
    "Djibouti",
    "Dominica",
    "Dominican Republic",
    "Ecuador",
    "Egypt",
    "El Salvador",
    "Equatorial Guinea",
    "Estonia",
    "Ethiopia",
    "Falkland Islands",
    "Faroe Islands",
    "Fiji",
    "Finland",
    "France",
    "French Polynesia",
    "French West Indies",
    "Gabon",
    "Gambia",
    "Georgia",
    "Germany",
    "Ghana",
    "Gibraltar",
    "Greece",
    "Greenland",
    "Grenada",
    "Guam",
    "Guatemala",
    "Guernsey",
    "Guinea",
    "Guinea Bissau",
    "Guyana",
    "Haiti",
    "Honduras",
    "Hong Kong",
    "Hungary",
    "Iceland",
    "India",
    "Indonesia",
    "Iran",
    "Iraq",
    "Ireland",
    "Isle of Man",
    "Israel",
    "Italy",
    "Jamaica",
    "Japan",
    "Jersey",
    "Jordan",
    "Kazakhstan",
    "Kenya",
    "Kuwait",
    "Kyrgyz Republic",
    "Laos",
    "Latvia",
    "Lebanon",
    "Lesotho",
    "Liberia",
    "Libya",
    "Liechtenstein",
    "Lithuania",
    "Luxembourg",
    "Macau",
    "Macedonia",
    "Madagascar",
    "Malawi",
    "Malaysia",
    "Maldives",
    "Mali",
    "Malta",
    "Mauritania",
    "Mauritius",
    "Mexico",
    "Moldova",
    "Monaco",
    "Mongolia",
    "Montenegro",
    "Montserrat",
    "Morocco",
    "Mozambique",
    "Namibia",
    "Nepal",
    "Netherlands",
    "Netherlands Antilles",
    "New Caledonia",
    "New Zealand",
    "Nicaragua",
    "Niger",
    "Nigeria",
    "Norway",
    "Oman",
    "Pakistan",
    "Palestine",
    "Panama",
    "Papua New Guinea",
    "Paraguay",
    "Peru",
    "Philippines",
    "Poland",
    "Portugal",
    "Puerto Rico",
    "Qatar",
    "Reunion",
    "Romania",
    "Russia",
    "Rwanda",
    "Saint Pierre &amp; Miquelon",
    "Samoa",
    "San Marino",
    "Satellite",
    "Saudi Arabia",
    "Senegal",
    "Serbia",
    "Seychelles",
    "Sierra Leone",
    "Singapore",
    "Slovakia",
    "Slovenia",
    "South Africa",
    "South Korea",
    "Spain",
    "Sri Lanka",
    "St Kitts &amp; Nevis",
    "St Lucia",
    "St Vincent",
    "St. Lucia",
    "Sudan",
    "Suriname",
    "Swaziland",
    "Sweden",
    "Switzerland",
    "Syria",
    "Taiwan",
    "Tajikistan",
    "Tanzania",
    "Thailand",
    "Timor L'Este",
    "Togo",
    "Tonga",
    "Trinidad &amp; Tobago",
    "Tunisia",
    "Turkey",
    "Turkmenistan",
    "Turks &amp; Caicos",
    "Uganda",
    "Ukraine",
    "United Arab Emirates",
    "United Kingdom",
    "Uruguay",
    "Uzbekistan",
    "Venezuela",
    "Vietnam",
    "Virgin Islands (US)",
    "Yemen",
    "Zambia",
    "Zimbabwe"
  );

  // Create cities objects
  for (let i = 0; i < 1000; i++) {
    data.cities.push(chance.city());
  }

  // Create locations objects
  for (let i = 0; i < 6; i++) {
    data.locations.push({
      id: i,
      name: `${chance.country()} Fitness Club`,
      description: chance.paragraph({ sentences: 3 }),
      email: chance.email(),
      phone: chance.phone(),
      country: chance.pickset(data.countries, 1).toString(),
      city: chance.pickset(data.cities, 1).toString(),
      street: chance.street(),
      building: chance.prime({ min: 10, max: 100 }),
      postalCode: chance.prime({ min: 100000, max: 500000 }),
      devices: chance.unique(chance.integer, 3, { min: 0, max: 4 }),
      address: chance.address(),
      devicesNumber: chance.prime({ min: 0, max: 5 }),
      employeesNumber: chance.prime({ min: 0, max: 20 }),
      membersNumber: chance.prime({ min: 0, max: 200 }),
    });
  }

  // Create members objects
  for (let i = 0; i < 20; i++) {
    data.members.push({
      id: i,
      firstName: chance.first(),
      lastName: chance.last(),
      height: chance.prime({ min: 150, max: 220 }),
      weight: chance.prime({ min: 40, max: 200 }),
      email: chance.email(),
      phone: chance.phone(),
      password: chance.string(),
      company: chance.company(),
      address: chance.address(),
      trainingsTotal: chance.prime({ min: 10, max: 20 }),
      trainingsDone: chance.prime({ min: 1, max: 10 }),
      reuseQuotient: chance.floating({ min: 0, max: 1 }),
      startDate: chance.date({ string: true, year: 2022 }),
      endDate: chance.date({ string: true, year: 2022 }),
      extensionDate: chance.date({ string: true, year: 2022 }),
      wellBeing: chance.prime({ min: 0, max: 2 }),
      nextRecheck: chance.date({ string: true, year: 2022 }),
    });
  }

  // Create employees objects
  for (let i = 0; i < 10; i++) {
    const fname = chance.first();
    const lname = chance.last();
    const color = chance.color({ format: "hex" }).slice(1);
    data.employees.push({
      id: i,
      firstName: fname,
      lastName: lname,
      avatar: `https://ui-avatars.com/api/?background=${color}&name=${fname}+${lname}`,
      email: chance.email(),
      selectedLocations: [data.locations[0].name],
      selectedCompetences: [data.competences[0]],
      phone: chance.phone(),
      // birthday: chance.date({year: 1978}),
      birthday: "2022/03/03",
      country: chance.pickset(data.countries, 1),
      city: chance.pickset(data.cities, 1),
      street: chance.street(),
      building: chance.prime({ min: 10, max: 100 }),
      postalCode: chance.prime({ min: 100000, max: 500000 }),
    });
  }

  for (let i = 0; i < 5; i++) {
    data.devices.push({
      id: i,
      name: "Device " + chance.hash({ length: 7 }),
      uniqueNumber: chance.hash({ length: 7 }),
      lastCheckDate: chance.date({ string: true, year: 2022 }),
      warrantyTill: chance.date({ string: true, year: 2022 }),
      workingHours: [
        {
          weekday: "Мonday",
          periods: [
            { start: "10:00", finish: "13:00" },
            { start: "14:00", finish: "18:00" },
          ],
        },
        {
          weekday: "Wednesday",
          periods: [{ start: "10:00", finish: "18:30" }],
        },
        {
          weekday: "Friday",
          periods: [
            { start: "10:00", finish: "11:30" },
            { start: "12:00", finish: "14:30" },
            { start: "15:00", finish: "17:30" },
          ],
        },
      ],
      exceptions: [
        {
          weekday: "05/13/2022",
          periods: [{ start: "10:00", finish: "19:30" }],
        },
      ],
    });
  }

  for (let i = 0; i < 6; i++) {
    const companyName = chance.company();
    data.companies.push({
      id: i,
      name: companyName,
      logo: `https://ui-avatars.com/api/?name=${i}+${companyName}`,
      email: chance.email(),
      phone: chance.phone(),
      website: chance.url(),
      country: chance.pickset(data.countries, 1),
      city: chance.pickset(data.cities, 1),
      street: chance.street(),
      building: chance.prime({ min: 10, max: 100 }),
      postalCode: chance.prime({ min: 100000, max: 500000 }),
      taxNumber: chance.guid(),
      registrationNumber: chance.guid(),
      membership: chance.prime({ min: 0, max: 2 }),
      branch: chance.pickset(
        ["Fitness club", "Club", "Lounge caffe", "Gym"],
        1
      ),
    });
  }

  return data;
};
